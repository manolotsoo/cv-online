import React, { useState } from 'react';
import Header from './../../components/header/Header.component';
import Content from './../../components/content/content.component';
import LatestProject from './../../components/content/latest-project.component';
import MyGitHub from './../../components/content/my-git-hub.component';
import Footer from '../../components/footer/Footer.component';
import { Col, Row } from 'react-bootstrap';
import Skills from '../../components/content/skills-posted.component';
import SkillsOther from '../../components/content/skills-other-tool-developper.component';
import Education from '../../components/content/education.component';
import WorkExperience from "../../components/content/work-experience.component";
import './App.css';

import './App.css';
interface IAppProps {
}

const App: React.FunctionComponent<IAppProps> = () => {

  // const pdfExportComponent = React.useRef(null);

  // const exportPDFWithMethod = () => {
  //   let element = document.getElementById('toExport') || document.body;
  //   savePDF(element, {
  //     paperSize: "A4",
  //   });
  // };

  return (
    <>
      <Header />
      <Content>
        <Row>
          <Col lg={7} xs={12}>
            {/* <WorkExperience title="Expérience professionnelle" /> */}
            <LatestProject title="Projets récents" />
            {/* <WorkExperience title="My work experience" /> */}
            <Education title="Parcours académique et formation" />
            {/* <MyGitHub title="Mes contributions github" /> */}
          </Col>
          {/* side right */}
          <Col lg={5} xs={12}>
            <Skills title="Compétences en javascript" />
            <SkillsOther title="Autres Compétences" />
          </Col>
        </Row>
      </Content>
      <Footer />
    </>
  );
};

export default App;