import * as React from 'react';

interface IIconProps {
  name: string
}

const Icon: React.FunctionComponent<IIconProps> = ({name}) => {
  return (
    <>
      <i className={'fa fa-'+ name}></i>
    </>
  )
};

export default Icon;
