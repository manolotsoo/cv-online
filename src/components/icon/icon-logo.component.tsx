import * as React from 'react';

interface IIconLogoProps {
  path: string
  alt: string
  width: number
}

const IconLogo: React.FunctionComponent<IIconLogoProps> = ({path, alt, width}) => {
  return (
    <>
      <img src={path} alt={alt} style={{"width": width}} />
    </>
  )
};

export default IconLogo;
