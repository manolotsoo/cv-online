import * as React from 'react';
import './Header.component.css';
import { Container, Row, Col, Image } from 'react-bootstrap';
import pdp from '../../assets/image/pdp.jpg';
import PersonalAddress from "../content/personal-address.component";
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { faCoffee } from '@fortawesome/free-solid-svg-icons'
import AboutMe from './../../components/content/about-me.component';

interface IAppProps {
}

const Header: React.FunctionComponent<IAppProps> = (props) => {
  return (
    <header className="header">
      <Container className="clearfix p-4">
        <Row>
          <Col md={2}>
            <Image src={pdp} style={{ width: "140px" }} alt="profile's picture" roundedCircle />
          </Col>
          <Col md={6}>
            <div className="d-flex h-100 justify-content-between profile-content" style={{ flexDirection: "column" }} >
              <h1 className="name">Manolotsoa Razafindrakoto</h1>
              <h2 className="desc">Dévéloppeur Web</h2>
              <AboutMe />
            </div>
          </Col>
          <Col md={4}>
            {/* <a href="/" className="btn btn-cta-primary float-right">Contactez-moi</a> */}
            <PersonalAddress />
          </Col>
        </Row>
        {/* list's logo of social networks */}
      </Container>
    </header>
  );
};

export default Header;
