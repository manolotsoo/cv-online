import * as React from 'react';

interface ILatestBlogPostProps {
}

const LatestBlogPost: React.FunctionComponent<ILatestBlogPostProps> = (props) => {
  return (
    <>
      Latest blog post
    </>
  );
};

export default LatestBlogPost;
