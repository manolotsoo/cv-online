import * as React from 'react';

interface IWorkExperienceProps {
  title: string
}

const WorkExperience: React.FunctionComponent<IWorkExperienceProps> = ({ title }) => {
  return (
    <>
      <section className="work-experience section">
        <div className="section-inner shadow-sm rounded">
          <h2 className="heading">
            <i className="fa fa-building"></i> {title}
          </h2>
          <div className="content">
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatum sit deleniti a quod suscipit quasi, asperiores error molestias aliquid earum eius neque culpa quisquam hic dolorem quibusdam ex enim deserunt.</p>
          </div>
        </div>
      </section>
    </>
  );
};

export default WorkExperience;
