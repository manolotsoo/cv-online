import * as React from 'react';

interface IAboutMeProps {
  title?: string
}

const AboutMe: React.FunctionComponent<IAboutMeProps> = ({title}) => {
  return (
    <section className="about section">
      {/* <div className="shadow-sm rounded"> */}
        {/* <h2 className="heading">
          {title}
        </h2> */}
        <div className="content">
          <p>Autodidacte, grande
          adaptabilité à la plupart des
          langages informatiques, une
          connaissance en
          mathématiques et passioné
        de l'algorithme.</p>
        </div>
      {/* </div> */}
    </section>
  );
};

export default AboutMe;
