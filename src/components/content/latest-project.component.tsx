import * as React from 'react';
import CardProject from "../card/latest-project-card.component";
import talentLogo from "./../../assets/image/projects/talenta-logo.png";
import doctelloLogo from "./../../assets/image/projects/medicali-logo.png";

interface ILatestProjectProps {
  title: string
}

const LatestProject: React.FunctionComponent<ILatestProjectProps> = ({ title }) => {
  return (
    <>
      <section className="latest-project section">
        <div className="section-inner shadow-sm rounded">
          <h3 className="heading">
          <i className="fa fa-tasks"></i> {title}
          </h3>
          <div className="content">
            <CardProject
              iconName="user"
              title="Mon CV en ligne"
              technology="ReactJS"
              description="Une page regroupant mon parcours, mes compétences ainsi que mes projets personnels."
              link="https://manou-cv-online.herokuapp.com/"
            />
            <CardProject
              title="Talenta Sourcing"
              // language="Javascript"
              technology="Angular et NodeJS"
              description="c'est une plateforme de recrutement destiné à toutes les entreprises sans exception à fin de selectionner à distance les profils compétents et motivés grâce aux tests sur mesure à l’image de l’entreprise."
              link="https://talent-sourcing.herokuapp.com"
              logoPath={talentLogo}
              logoWidth={44}
            />
            <CardProject
              title="Doctello"
              // language="Javascript"
              technology="Javascript"
              description="c'est une plateforme de prise de rendez-vous en ligne créé pour les médecins, dans l’intérêt des médecins et au service des patients. Il est entièrement financé par la participation des médecins. Aucune levée de fond n’est faite auprès des professionnels de la finance."
              logoPath={doctelloLogo}
              logoWidth={44}
            />
          </div>
        </div>
      </section>
    </>
  );
};

export default LatestProject;
