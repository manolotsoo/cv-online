import * as React from 'react';

interface IPersonalAddressProps {
  title?: string
}

const PersonalAddress: React.FunctionComponent<IPersonalAddressProps> = ({ title }) => {
  return (
    <>
      <aside className="h-100">
        <div className="info aside section h-100">
          <div className="section-inner rounded h-100">
            {title && <h2 className="heading">
              {title}
            </h2>}
            <ul className="list-unstyled h-100 d-flex justify-content-around" style={{ flexDirection: "column" }}>
              <li><i className="fa fa-map-marker"></i> Lot II A B 22 Bis Andrononobe</li>
              <li><i className="fa fa-envelope"></i> manolotsoadaniel@gmail.com</li>
              <li><i className="fa fa-phone"></i> +261 34 70 653 01</li>
              <li><i className="fa fa-github"></i><a href="https://github.com/manoulotsoa"> https://github.com/manoulotsoa</a></li>
              <li><i className="fa fa-gitlab"></i><a href="https://gitlab.com/manolotsoo"> https://gitlab.com/manolotsoo</a></li>
              {/* <li><i className="fa fa-linkedin"></i> Manolotsoa Razafindrakoto</li> */}
            </ul>
          </div>
        </div>
      </aside>
    </>
  )
};

export default PersonalAddress;
