import * as React from 'react';
import CardEducation from "../card/education-card.component";
import ituLogo from "../../assets/image/academical/itu.png";
import facScienceLogo from "../../assets/image/academical/logo-FAC-SCIENCE.png";

interface IEducationProps {
  title: string
  icon?: JSX.Element
}

const Education: React.FunctionComponent<IEducationProps> = ({ icon, title }) => {
  return (
    <>
      <section className="education section">
        <div className="section-inner shadow-sm rounded">
          <h2 className="heading">
            <i className="fa fa-graduation-cap"></i> {title}
          </h2>
          <div className="content">
            <CardEducation
              logoPath={ituLogo}
              logoWidth={44}
              altLogo="Itu's logo"
              title="Information Technologie University"
              description="C'est une formation en mathématiques et informatiques pour être developpeur fullstack."
              date="2014-2016 / 2021"
            />
            <CardEducation
              logoPath={facScienceLogo}
              logoWidth={44}
              altLogo="Math's logo"
              title="Mathématiques"
              date="2012-2014"
            />
          </div>
        </div>
      </section>
    </>
  );
};

export default Education;
