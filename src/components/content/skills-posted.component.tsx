import * as React from 'react';
import { ProgressBar } from 'react-bootstrap';
import { Col, Row } from 'react-bootstrap';
import expressLogo from "./../../assets/image/skillsJS/express-logo.png";
import angularLogo from "./../../assets/image/skillsJS/angular.png";
import nodejsLogo from "./../../assets/image/skillsJS/nodeJS-logo.png";
import reactjsLogo from "./../../assets/image/skillsJS/reactJS-logo.png";
import jqueryLogo from "./../../assets/image/skillsJS/jquery-logo.png";


interface ISkillsProps {
  title: string;
}

const Skills: React.FunctionComponent<ISkillsProps> = ({ title }) => {
  return (
    <>
        <aside className="skills aside section">
          <div className="div section-inner rounded">
            <h2 className="heading">
            <i className="fa fa-code"></i> {title}
            </h2>
            <div className="content">
              <p className="intro"></p>
              <div className="skillset developper-skills-list py-2">
                <Row>
                  <Col lg={12} xs={12} className="my-1">
                  <div className="item d-flex">
                      <div className="icon col-lg-3 col-3">
                        <img src={angularLogo} alt="angular" title="angular" width="60px" />
                      </div>
                      <div className="progressbar col-lg-9 py-4">
                        <ProgressBar now={60} />
                      </div>
                    </div>
                  </Col>
                  <Col lg={12} xs={12} className="my-1">
                    <div className="item d-flex">
                      <div className="icon col-lg-3 col-3">
                        <img src={expressLogo} alt="expressLogo" title="expressJS" width="60px" />
                      </div>
                      <div className="progressbar col-lg-9 py-4">
                        <ProgressBar now={60} />
                      </div>
                    </div>
                  </Col>
                  <Col lg={12} xs={12} className="my-1">
                    <div className="item d-flex">
                      <div className="icon col-lg-3 col-3">
                        <img src={nodejsLogo} alt="nodejsLogo" title="nodeJS" width="60px" />
                      </div>
                      <div className="progressbar col-lg-9 py-4">
                        <ProgressBar now={80} />
                      </div>
                    </div>
                  </Col>
                  <Col lg={12} xs={12} className="my-1">
                    <div className="item d-flex">
                      <div className="icon col-lg-3 col-3">
                        <img src={reactjsLogo} alt="reactJSLogo" title="reactJS" width="60px" />
                      </div>
                      <div className="progressbar col-lg-9 py-4">
                        <ProgressBar now={55} />
                      </div>
                    </div>
                  </Col>
                  <Col lg={12} xs={12} className="my-1">
                    <div className="item d-flex">
                      <div className="icon col-lg-3 col-3">
                        <img src={jqueryLogo} alt="jQueryLogo" title="jQuery" width="60px" />
                      </div>
                      <div className="progressbar col-lg-9 py-4">
                        <ProgressBar now={80} />
                      </div>
                    </div>
                  </Col>
                </Row>
              </div>
            </div>
          </div>
        </aside>
    </>
  )
};

export default Skills;
