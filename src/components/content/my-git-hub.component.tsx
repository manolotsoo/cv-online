import * as React from 'react';
import GitHubCalendar from 'react-github-calendar';

interface IMyGitHubProps {
  title: string
}

const MyGitHub: React.FunctionComponent<IMyGitHubProps> = ({ title }) => {
  return (
    <>
      {/* <Row> */}
      {/* <Col lg={8} xs={12}> */}
      <section className="latest-project section">
        <div className="section-inner shadow-sm rounded">
          <h2 className="heading">
          <i className="fa fa-github"></i> {title}
          </h2>
          <div className="content" style={{ backgroundColor: "lightblue" }}>
            <GitHubCalendar username="manoulotsoa" />
            {/* <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatum sit deleniti a quod suscipit quasi, asperiores error molestias aliquid earum eius neque culpa quisquam hic dolorem quibusdam ex enim deserunt.</p> */}
          </div>
        </div>
      </section>
      {/* </Col> */}
      {/* </Row> */}
    </>
  );
};

export default MyGitHub;
