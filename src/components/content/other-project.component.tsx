import * as React from 'react';

interface IOtherProjectProps {
}

const OtherProject: React.FunctionComponent<IOtherProjectProps> = (props) => {
  return (
    <>
      Other Project
    </>
  );
};

export default OtherProject;
