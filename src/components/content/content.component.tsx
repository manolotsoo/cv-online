import * as React from 'react';
import { Container } from 'react-bootstrap';

interface IContentProps {
}

const Content: React.FunctionComponent<IContentProps> = ({ children }) => {
  return (
    <>
      <Container className="sections-wrapper">
        {children}
      </Container>
    </>
  )
};

export default Content;
