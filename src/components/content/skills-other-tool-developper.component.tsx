import * as React from 'react';
import { Image, Row, Col } from "react-bootstrap";
import javascriptLogo from '../../assets/image/skills_done/javascript-logo.png';
import htmlLogo from '../../assets/image/skills_done/html5-logo.png';
import githubLogo from '../../assets/image/skills_done/github-logo.png';
import gitlabLogo from '../../assets/image/skills_done/gitlab-logo.png';
import gitLogo from '../../assets/image/skills_done/git-logo.png';
import trelloLogo from '../../assets/image/skills_done/trello-logo.png';
import cssLogo from '../../assets/image/skills_done/css-logo.png';
import windowsLogo from '../../assets/image/skills_done/windows-logo.gif';
import linuxLogo from '../../assets/image/skills_done/linux-logo.png';
import javaLogo from '../../assets/image/skills_done/java-logo.png';
import phpLogo from '../../assets/image/skills_done/php-logo.png';
import mysqlLogo from '../../assets/image/skills_done/mysql-logo.png';
import postgresqlLogo from '../../assets/image/skills_done/postgresql-logo.png';
import restLogo from '../../assets/image/skills_done/rest-logo.png';
import graphqlLogo from '../../assets/image/skills_done/graphql-logo.png';
import bootstrapLogo from '../../assets/image/skills_done/bootstrap-logo.png';
import materialAngularLogo from '../../assets/image/skills_done/angular-material-logo.png';
import fontAwesomeLogo from '../../assets/image/skills_done/font-awesome-logo.png';
import './skills-other-tool-developper.component.css';

interface ISkillsOtherProps {
  title: string
}

const SkillsOther: React.FunctionComponent<ISkillsOtherProps> = ({title}) => {
  return (
    <>
      <aside className="skills aside section">
          <div className="div section-inner shadow-sm rounded">
            <h2 className="heading">
            <i className="fa fa-code"></i> {title}
            </h2>
            <div className="content developper-skills-list">
              <div className="item-sub-menu my-3">
                <h4><i className="fa fa-wrench"></i> Outils de dévéloppement</h4>
                <div className="skillset pl-4">
                  <Row>
                    <Col lg={3} xs={3}>
                      <div className="item">
                        <Image src={htmlLogo} style={{ width: "64px" }} title="HTML 5" alt="html's picture" />
                      </div>
                    </Col>
                    <Col lg={3} xs={3}>
                      <div className="item">
                        <Image src={cssLogo} style={{ width: "64px" }} title="CSS 3" alt="css's picture" />
                      </div>
                    </Col>         
                    <Col lg={3} xs={3}>
                      <div className="item">
                        <Image src={javascriptLogo} style={{ width: "64px" }} title="javascript" alt="javascript's picture" />
                      </div>
                    </Col>
                    <Col lg={3} xs={3}>
                      <div className="item">
                        <Image src={githubLogo} style={{ width: "64px" }} title="github" alt="github's picture" />
                      </div>
                    </Col>
                    <Col lg={3} xs={3}>
                      <div className="item">
                        <Image src={gitlabLogo} style={{ width: "64px" }} title="gitlab" alt="gitlab's picture" />
                      </div>
                    </Col>
                    <Col lg={3} xs={3}>
                      <div className="item">
                        <Image src={gitLogo} style={{ width: "64px" }} title="git" alt="gitlab's picture" />
                      </div>
                    </Col>
                    <Col lg={3} xs={3}>
                      <div className="item">
                        <Image src={trelloLogo} style={{ width: "64px" }} title="trello" alt="trello's picture" />
                      </div>
                    </Col>
                    <Col lg={3} xs={3}>
                      <div className="item">
                        <Image src={javaLogo} style={{ width: "64px" }} title="java" alt="java's picture" />
                      </div>
                    </Col>
                    <Col lg={3} xs={3}>
                      <div className="item">
                        <Image src={phpLogo} style={{ width: "64px" }} title="php" alt="php's picture" />
                      </div>
                    </Col>
                    <Col lg={3} xs={3}>
                      <div className="item">
                        <Image src={bootstrapLogo} style={{ width: "64px" }} title="bootstrap" alt="bootstrap's picture" />
                      </div>
                    </Col>
                    <Col lg={3} xs={3}>
                      <div className="item">
                        <Image src={fontAwesomeLogo} style={{ width: "64px" }} title="font-awesome" alt="font-awesome's picture" />
                      </div>
                    </Col>
                    <Col lg={3} xs={3}>
                      <div className="item">
                        <Image src={materialAngularLogo} style={{ width: "64px" }} title="material-angular" alt="material-angular's picture" />
                      </div>
                    </Col>
                  </Row>
                </div>
              </div>
              <div className="item-sub-menu my-3">
                <h4><i className="fa fa-database"></i> Base de données</h4>
                  <div className="skillset pl-4">
                    <Row>
                      <Col lg={3} xs={3}>
                        <div className="item">
                          <Image src={mysqlLogo} style={{ width: "64px" }} title="mysql" alt="mysql's picture" />
                        </div>
                      </Col>
                      <Col lg={3} xs={3}>
                        <div className="item">
                          <Image src={postgresqlLogo} style={{ width: "64px" }} title="Postgresql" alt="postgresql's picture" />
                        </div>
                      </Col>
                    </Row>
                  </div>
              </div>
              <div className="item-sub-menu my-3">
                <h4><i className="fa fa-server"></i> Service Web</h4>
                  <div className="skillset pl-4">
                    <Row>
                      <Col lg={3} xs={3}>
                        <div className="item">
                          <Image src={restLogo} style={{ width: "64px" }} title="REST" alt="rest's picture" />
                        </div>
                      </Col>
                      <Col lg={3} xs={3}>
                        <div className="item">
                          <Image src={graphqlLogo} style={{ width: "64px" }} title="GRAPHQL" alt="graphql's picture" />
                        </div>
                      </Col>
                    </Row>
                  </div>
              </div>
              <div className="item-sub-menu my-3">
                <h4><i className="fa fa-laptop"></i> Système d'exploitation</h4>
                  <div className="skillset pl-4">
                    <Row>
                      <Col lg={3} xs={3}>
                        <div className="item">
                          <Image src={windowsLogo} style={{ width: "64px" }} title="Windows" alt="html's picture" />
                        </div>
                      </Col>
                      <Col lg={3} xs={3}>
                        <div className="item">
                          <Image src={linuxLogo} style={{ width: "64px" }} title="Linux GNU" alt="linux's picture" />
                        </div>
                      </Col>
                    </Row>
                  </div>
              </div>
            </div>
          </div>
        </aside>
    </>
  );
};

export default SkillsOther;
