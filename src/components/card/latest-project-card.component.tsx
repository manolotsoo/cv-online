import * as React from 'react';

interface ICardLatestProjectProps {
  title: string
  technology: string
  description: string
  link?: string
  iconName?: string
  logoPath?: string
  logoWidth?: number
}

const CardLatestProject: React.FunctionComponent<ICardLatestProjectProps> = ({ title, technology, description, link, iconName, logoPath, logoWidth }) => {
  return (
    <>
      <div className="card mb-3">
        <div className="card-body">
          <h5 className="card-title">
            {
              logoWidth 
              ?
                logoPath && <img src={logoPath} alt="project's image" width={logoWidth + 'px'} />
              :
                logoPath && <img src={logoPath} alt="project's image" />
            }
            {iconName && <i className={"fa fa-"+iconName}></i>} {title}
          </h5>
          {/* <h6 className="card-subtitle mb-2 text-muted"><b>Language:</b> {language}</h6> */}
          <h6 className="card-subtitle mb-2 text-muted"><b>Technologies:</b> {technology}</h6>
          <p className="card-text"><b>Description: </b>{description}</p>
          {
            link &&
            <>
              <b>lien: </b>
              <a href={link} className="card-link">{link}</a>
            </>
          }
        </div>
      </div>
    </>
  )
};

export default CardLatestProject;
