import * as React from 'react';

interface ICardEducationProps {
  title: string
  description?: string
  date: string
  logoPath?: string
  logoWidth?: number
  altLogo?: string
}

const CardEducation: React.FunctionComponent<ICardEducationProps> = ({ title, description, date, logoPath, logoWidth, altLogo }) => {
  return (
    <>
      <div className="card">
        <div className="card-body">
          <h5 className="card-title">
           {
              logoWidth 
              ?
                logoPath && <img src={logoPath} alt={altLogo} width={logoWidth + 'px'} />
              :
                logoPath && <img src={logoPath} alt={altLogo} />
            } {title}
          </h5>
          <h6 className="card-subtitle mb-2 text-muted"><b>Date: </b>{date}</h6>
          {
            description &&
            <p>
              {description}
            </p>
          }
        </div>
      </div>
    </>
  )
};

export default CardEducation;
