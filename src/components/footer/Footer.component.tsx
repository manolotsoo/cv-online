import * as React from 'react';
import { Container } from 'react-bootstrap';
import './Footer.component.css';

interface IFooterProps {
}

const Footer: React.FunctionComponent<IFooterProps> = () => {
  return (
    <>
      <footer className="footer">
        <Container className="text-center">
          Manolotsoa Razafindrakoto
          <small className="copyright">
            &nbsp;copyright 2020
          </small>
        </Container>
      </footer>
    </>
  );
};

export default Footer;
